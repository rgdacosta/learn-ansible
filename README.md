# learn-ansible

===
## Get a good editor

# Visual Studio Code
https://code.visualstudio.com/

# Atom
https://atom.io/

# Editors for Ansible playbooks:

https://opensource.com/article/20/6/open-source-alternatives-vs-code

https://www.ansible.com/resources/webinars-training/using-new-vs-code-extension-for-ansible

https://dev.to/thegeoffstevens/vs-code-settings-you-should-customize-5e75

===

## Best Practices:
https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html

===

## Variables

# 22 Different ways to use variables
https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable

# Special variables (including magic variables):
https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html

# Local facts:
https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#local-facts-facts-d

===

## Filters

# jinja2 filters:
https://jinja.palletsprojects.com/en/2.11.x/templates/#filters

# ipaddr filter:
https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters_ipaddr.html

===

## Loops

https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html

# Migrating from with_X to loop:
https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html#migrating-to-loop

===

# Conditionals:
https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html

===

## Jinja2

https://jinja.palletsprojects.com/en/2.10.x/
https://jinja.palletsprojects.com/en/2.10.x/templates/#builtin-filters
https://ansible-docs.readthedocs.io/zh/stable-2.0/rst/playbooks_filters.html#list-filters
https://blog.codecentric.de/en/2014/08/jinja2-better-ansible-playbooks-templates/

#Jinja2 conditionals:
https://jinja.palletsprojects.com/en/master/templates/#comparisons

===

## Application Deployment with Ansible:
https://www.ansible.com/use-cases/application-deployment

===

## Ansible for Network Automation case studies:

# How Ansible for Networking is different:
https://docs.ansible.com/ansible/latest/network/getting_started/network_differences.html

https://www.redhat.com/cms/managed-files/rh-swisscom-case-study-f13907cw-201809-en.pdf

https://www.redhat.com/cms/managed-files/rh-surescripts-healthcare-case-study-f11282jm-201803-en.pdf

===

# Play blocks:
https://docs.ansible.com/ansible/latest/user_guide/playbooks_blocks.html


